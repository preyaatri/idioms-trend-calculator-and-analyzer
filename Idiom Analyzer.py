import docx
from docx import Document
import os
import subprocess
import collections
import glob
import sys
import string
import enchant
import csv

####document = docx.Document('Dictionary1.docx')
##document = docx.Document('FinalEnglishIdiomsdictionary.docx')
document = docx.Document('pages_dictionary.docx')

bolds=[]
meaning = []
meaning2 = []
meaning3 = []
meaning4 = []
meaning5 = []
italics=[]
txtfiles = []
i=0
indicator = 0
index = 0

break_indicator = 0
idiom_words = []

for para in document.paragraphs:
    a = 0
    m2 = ""
    m3 = ""
    m4 = ""
    m5 = ""
    break_indicator_m3 = 0
    break_indicator_m4 = 0
    break_indicator_m5 = 0
    idiom_words = []
    break_indicator_m2 = 0
    i=i+1
    if i>=17:
        for run in para.runs:
            if run.bold:
                run.text = ''.join([i for i in run.text if not i.isdigit()])
                run.text = ''.join([i for i in run.text if not i == '.'])
                if len(run.text) > 2:
                    run.text = run.text.rstrip()
                    if run.text not in bolds:
                        bolds.append(run.text)
                        index = index + 1
            elif not run.bold and not run.italic:
                run.text = ''.join([i for i in run.text if not i.isdigit()])
#                run.text = ''.join([i for i in run.text if not i == '.'])
                if len(run.text) > 3:
                    run.text = run.text.rstrip()
                    word_list = run.text.split()
                    last = len(word_list) - 1
                    if "See" == word_list[0] or "Copyright" == word_list[0]:
                        continue
                    if "AnD" == word_list[0]:
                        indicator = 1
                        continue
                    if ";" in run.text:
                        run.text,m2 = run.text.split(';',1)
                        if ";" in m2:
                            m2,m3 = m2.split(';',1)
                            if ";" in m3:
                                m3,m4 = m3.split(';',1)
                                if ";" in m4:
                                    m4,m5 = m4.split(';',1)
                        if m2 != "":
                            break_indicator_m2 = 1
                            meaning2.append(m2)
                        if m3 != "":
                            break_indicator_m3 = 1
                            meaning3.append(m3)
                        if m4 != "":
                            break_indicator_m4 = 1
                            meaning4.append(m4)
                        if m5 != "":
                            break_indicator_m5 = 1
                            meaning5.append(m5)
                    meaning.append(run.text)
                    if break_indicator_m2 == 0:
                        meaning2.append("")
                    if break_indicator_m3 == 0:
                        meaning3.append("")
                    if break_indicator_m4 == 0:
                        meaning4.append("")
                    if break_indicator_m5 == 0:
                        meaning5.append("")
                    if indicator == 1:
                        meaning.append(run.text)
                        indicator = 0

            
import pandas as pd
from pandas import DataFrame
##print(bolds)
##print(meaning)

##zip(bolds,meaning)
##with open('Idioms.csv','w') as f:
##    writer = csv.writer(f, delimiter='\t')
##    writer.writerows(zip(bolds,meaning))

df1 = DataFrame(meaning)
df = DataFrame(bolds)
df2 = DataFrame(meaning2)
df3 = DataFrame(meaning3)
df4 = DataFrame(meaning4)
df5 = DataFrame(meaning5)
ct = pd.concat([df,df1,df2,df3,df4,df5], axis = 1)
##print(ct)

ct.to_csv('Idioms.csv', index = None, header = None, encoding='utf-8-sig')

file_names = []
number_of_idioms = []
for f in glob.glob("Docx Files/*.docx"):
    file_names.append(os.path.basename(f))

for f_name in file_names:
    file = docx.Document("Docx Files/"+f_name)
    l = []
    for para1 in file.paragraphs:
        for run in para1.runs:
            for idiom in bolds:
                if idiom in run.text:
                    l.append(idiom)
    number_of_idioms.append(len(l))
    counter = collections.Counter(l)
    print(f_name)
    print()
    print(counter)
    print()
    print()

wordcountdict = {}
number_of_words = []
d = enchant.Dict("en_US")
for file in os.listdir("TXT Files/"):
    if file.endswith(".txt"):
        words = []
        punct = ['\n','.',',','...']
        with open("TXT Files/"+file,'r', encoding='utf-8') as fi:
            for line in fi:
                for word in line.split(" "):
                    if(d.check(word)):
                       words.append(word)
        number_of_words.append(len(words))
        wordcountdict.update({file : len(words)})
##print(number_of_words)        
#print(wordcountdict)

######################################Printing##################################
for i, file in enumerate(file_names):
    print("File name -",file)
    print("Total number of Idioms contained = ",number_of_idioms[i])
    print("Total number of Words contained = ",number_of_words[i])
    print()


exit(0)
import pandas as pd
year = [1995,1996,1997,1998,1999,2000,2001,2002,2003,2004,2005,2006,2007,2008,2009,2010,2011,2012,2013,2014,2015,2016,2017,2018,2019,2020]
dictn = {
    "years": year,
    "filename":file_names,
    "idioms":number_of_idioms,
    "words": number_of_words
    }
frame = pd.DataFrame(dictn)
frame.to_csv("idiom_count.csv", index = False, encoding = "utf-8")




#Plotting
import numpy as np
import matplotlib.pyplot as plt
plt.style.use('ggplot')


plt.bar(file_names,number_of_idioms, color = 'green')
plt.title("Graph for Number of Idioms in each File")
plt.xlabel('File Names', fontsize=10, color='black')
plt.setp(plt.gca().get_xticklabels(), rotation=45, horizontalalignment='right')
plt.ylabel('Number of Idioms', fontsize=10, color='black')
plt.margins(0.2)
plt.legend(loc='best')
plt.savefig('Plot1.png', bbox_inches='tight')


plt.bar(file_names,number_of_words, color = 'blue')
plt.title("Graph for Number of Words in each File")
plt.xlabel('File Names', fontsize=10, color='black')
plt.setp(plt.gca().get_xticklabels(), rotation=45, horizontalalignment='right')
plt.ylabel('Number of Words', fontsize=10, color='black')
plt.margins(0.2)
plt.legend(loc='best')
plt.savefig('Plot2.png', bbox_inches='tight')



plt.bar(file_names,number_of_idioms,width=0.8, color = 'blue')
plt.bar(file_names,number_of_words,width=0.8, color = 'red')
plt.title("Graph for Number of Words VS Number of Idioms w.r.t Files")
plt.xlabel('File Names', fontsize=10, color='black')
plt.setp(plt.gca().get_xticklabels(), rotation=45, horizontalalignment='right')
plt.ylabel('Number of Words & Idioms', fontsize=10, color='black')
plt.margins(0.2)
plt.legend(loc='best')
plt.savefig('Plot3.png', bbox_inches='tight')



